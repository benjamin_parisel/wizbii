<?php

namespace AnaliticsAPI\Util;


class Helper_Var
{
    /**
     * Fonction permettant de savoir si la variable parametree est vide
     * @param mixed mVariable variable de type quelconque a tester
     * @return boolean <b>true</b> si la variable testee est vide, <b>false</b> si elle n'est pas vide
     */
    static public function isEmpty($mVariable) {
        // Si valeur nulle, celle ci est vide
        if ($mVariable === null) {
            return true;
        }

        // Test selon type variable
        if (is_string($mVariable)) { // Test si variable est de type String
            // Cas de la chaine vide
            if ($mVariable === "" || strlen($mVariable) === 0) {
                return true;
            }
        }
        else if (is_array($mVariable)) { // Test si variable est de type Array
            // Cas du tableau à 0 case
            if (count($mVariable) === 0){
                return true;
            }
        }
        else if (is_int($mVariable)){ /* Test si variable est de type entier*/ }
        else if (is_bool($mVariable)) { /* Test si variable est de type booleen */
        }
        else if (is_float($mVariable)) { /* Test si variable est de type float*/
        }
        else if (is_object($mVariable)) { /*Test si variable est de type objet*/
        }

        // Si on arrive ici, variable non vide
        return false;
    }

    /**
     * Fonction permettant de de lever une exception si la variable passee en parametre est vide.
     * @param mixed mVariable variable de type quelconque
     * @param string $sFailMsg le message d'erreur a afficher, par defaut : "isEmpty($mVariable) === true"
     * @throws \Exception si variable passee en parametre vide
     */
    static public function check($mVariable, $sFailMsg = 'isEmpty($mVariable) === true') {
        if (self::isEmpty($mVariable) === true) {
            throw new \Exception($sFailMsg);
        }
    }

}