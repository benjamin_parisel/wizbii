<?php
namespace AnaliticsAPI\Controller;

use AnaliticsAPI\Document\Content;
use AnaliticsAPI\Util\Helper_Var;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer as jmsSerialiser;
use Symfony\Component\HttpKernel\Exception\HttpException;
use AnaliticsAPI\Util\Constants;
use AnaliticsAPI\Util\ParameterHelper;

/**
 * Controller pour les documents
 * @package AnaliticsAPI\Controller
 */
class ContentController extends Controller
{

    /**
     * Listes tout les documents présent en base
     * @return Response
     */
    public function indexAction()
    {
        $content = $this->get('doctrine_mongodb')
            ->getRepository('AnaliticsAPI:Content')
            ->findAll();
        $serializer = jmsSerialiser\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($content, 'json');
        return new Response($jsonContent);

    }

    /**
     * Collect les informations et les stockes en base de données
     * @param Request $request
     * @return Response
     */
    public function collectAction(Request $request)
    {
        // Check du numéro de version
        if (Helper_Var::isEmpty($request->query->get(ParameterHelper::VERSION)) ||
            $request->query->get(ParameterHelper::VERSION) != ParameterHelper::version
        ) {
            throw new HttpException(400, 'API is on version ' . ParameterHelper::version);
        };

        if (!Helper_Var::isEmpty($request->query->get(ParameterHelper::QUEUE_TIME)) &&
            ($request->query->get(ParameterHelper::QUEUE_TIME) < 0 ||
                $request->query->get(ParameterHelper::QUEUE_TIME) > $this->container->getParameter('app.quota_qt'))
        ) {
            throw new HttpException(400, 'Queue Time must be >= 0 and < ' . $this->container->getParameter('app.quota_qt'));
        }

        // Construction des paramètres
        $params = ParameterHelper::loadArrayParam();
        foreach ($params as $key => $value) {
            $params[$key] = $request->query->get($key);
        }

        $serializer = jmsSerialiser\SerializerBuilder::create()->build();
        // Serialisation des params en Json
        $jsonContent = $serializer->serialize($params, 'json');


        // Conversion en objet
        $content = $serializer->deserialize($jsonContent, 'AnaliticsAPI\Document\Content', 'json');
        $this->checkIfParamMandotoryWasMissing($content);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($content);
        $dm->flush();

        return new Response($serializer->serialize($content, 'json'));
    }


    /**
     * Vérifie si le contenu dispose des éléments obligatoires
     * @throws \Exception
     */
    private function checkIfParamMandotoryWasMissing(Content $content)
    {
        Helper_Var::check($content);
        if (Helper_Var::isEmpty($content->getHitType()) ||
            Helper_Var::isEmpty($content->getTrackingId()) ||
            Helper_Var::isEmpty($content->getDataSource())
        ) {
            throw new HttpException(401, 'Missing mandatories parameters');
        }
        if (Helper_Var::isEmpty($content->getScreenName()) && $content->isMobile() && $content->getHitType() === 'screenview') {
            throw new HttpException(401, 'Missing mandatories parameters');
        }
        if ((Helper_Var::isEmpty($content->getEventCategory()) || Helper_Var::isEmpty($content->getEventAction()))
            && $content->getHitType() === Constants::EVENT
        ) {
            throw new HttpException(401, 'Missing mandatories parameters !');
        }
        if ($content->isMobile() && Helper_Var::isEmpty($content->getApplicationName())) {
            throw new HttpException(401, 'Application name can\'t be null !');
        }
        if (Helper_Var::isEmpty($content->getWizbiiCreator()) || ParameterHelper::isUserAvaible($content->getWizbiiCreator())) {
            if (!$content->isWeb()) {
                throw new HttpException(401, 'User don\'t be authorized to collect this data !');
            } else {
                // On récupère les informations depuis les cookies du site
                $content->setWizbiiCreator($_COOKIE['wc']);
                $content->setWizbiiCreatorType($_COOKIE['wct']);
            }
        }
    }

}