<?php
namespace AnaliticsAPI\Util;

/**
 * Classe contenant les paramètres de l'API
 * @package AnaliticsAPI\Util
 */
class ParameterHelper
{
    const VERSION = 'v';
    const HIT_TYPE = 't';
    const DOCUMENT_LOCATION = 'dl';
    const DOCUMENT_REFERER = 'dr';
    const WIZBII_CREATOR_TYPE = 'wct';
    const WIZBII_CREATOR = 'wci';
    const EVENT_CATEGORY = 'ec';
    const EVENT_ACTION = 'ea';
    const EVENT_LABEL = 'el';
    const EVENT_VALUE = 'ev';
    const TRACKING_ID = 'tid';
    const DATASOURCE = 'ds';
    const CAMPAIGN_NAME = 'cn';
    const CAMPAIGN_SOURCE = 'cs';
    const CAMPAIGN_MEDIUM = 'cm';
    const CAMPAIGN_KEYWORD = 'ev';
    const CAMPAIGN_CONTENT = 'cc';
    const SCREEN_NAME = 'sn';
    const APPLICATION_NAME = 'an';
    const APPLICATION_VERSION = 'av';
    const QUEUE_TIME = 'qt';
    const CACHE_BURSTER = 'z';
    const QUOTA_QT = 3600;
    const version =1;

    /**
     * @return array
     */
    static function loadArrayParam()
    {
        return array(self::VERSION => '', self::HIT_TYPE => '', self::DOCUMENT_LOCATION => '', self::DOCUMENT_REFERER => '', self::WIZBII_CREATOR => '',
            self::WIZBII_CREATOR_TYPE => '',
            self::EVENT_ACTION => '', self::EVENT_CATEGORY=> '', self::EVENT_LABEL=> '', self::EVENT_VALUE=> '',self::TRACKING_ID=> '',
            self::DATASOURCE => '',  self::CAMPAIGN_CONTENT => '',self::CAMPAIGN_KEYWORD => '',self::CAMPAIGN_MEDIUM => '',self::CAMPAIGN_SOURCE => '',
            self::CAMPAIGN_NAME  => '',  self::SCREEN_NAME => '',  self::APPLICATION_NAME => '',self::APPLICATION_VERSION=> '',self::QUEUE_TIME => '',self::CACHE_BURSTER => '');

    }

    /**
     * Indique si l'utilisateur fait partie des utilisateurs autorisés
     * @return boolean
     */
    static  function isUserAvaible($user){
        $users = array('john-doe','michel-dupont','bernard-micho');
        return in_array(str_replace($user,'-','_'),$users);
    }
}