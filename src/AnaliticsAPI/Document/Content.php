<?php
namespace AnaliticsAPI\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use AnaliticsAPI\Util\Constants;


/**
 * @MongoDB\Document()
 * ReferenceOne() Entity(repositoryClass="Wmd\WatchMyDeskBundle\Repository\DeskRepository")
 */
class Content
{
    /**
     * @MongoDB\Id
     * @SerializedName("id")
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("t")
     */
    protected $hitType;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("dl")
     */
    protected $documentLocation;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("dr")
     */
    protected $documentReferer;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("wct")
     */
    protected $WizbiiCreatorType;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("wci")
     */
    protected $WizbiiCreator;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("ec")
     */
    protected $eventCategory;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("ea")
     */
    protected $eventAction;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("el")
     */
    protected $eventLabel;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("ev")
     */
    protected $eventValue;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("tid")
     */
    protected $trackingId;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("ds")
     */
    protected $dataSource;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("cn")
     */
    protected $campaignName;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("cs")
     */
    protected $campaignSource;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("cm")
     */
    protected $campagnMedium;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("cc")
     */
    protected $campaignContent;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("sn")
     */
    protected $screenName;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("an")
     */
    protected $applicationName;

    /**
     * @MongoDB\Field(type="string")
     * @Type("string")
     * @SerializedName("av")
     */
    protected $applicationVersion;

    /**
     * Indique si le contenu vient d'une application mobile
     * @return bool
     */
    public function isMobile()
    {
        return $this->getDataSource() == Constants::APPS ? true : false;
    }
    /**
     * Indique si le contenu vient du web
     * @return bool
     */
    public function isWeb()
    {
        return $this->getDataSource() == Constants::WEB ? true : false;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hitType
     *
     * @param string $hitType
     * @return self
     */
    public function setHitType($hitType)
    {
        $this->hitType = $hitType;
        return $this;
    }

    /**
     * Get hitType
     *
     * @return string $hitType
     */
    public function getHitType()
    {
        return $this->hitType;
    }

    /**
     * Set documentLocation
     *
     * @param string $documentLocation
     * @return self
     */
    public function setDocumentLocation($documentLocation)
    {
        $this->documentLocation = $documentLocation;
        return $this;
    }

    /**
     * Get documentLocation
     *
     * @return string $documentLocation
     */
    public function getDocumentLocation()
    {
        return $this->documentLocation;
    }

    /**
     * Set documentReferer
     *
     * @param string $documentReferer
     * @return self
     */
    public function setDocumentReferer($documentReferer)
    {
        $this->documentReferer = $documentReferer;
        return $this;
    }

    /**
     * Get documentReferer
     *
     * @return string $documentReferer
     */
    public function getDocumentReferer()
    {
        return $this->documentReferer;
    }

    /**
     * Set wizbiiCreatorType
     *
     * @param string $wizbiiCreatorType
     * @return self
     */
    public function setWizbiiCreatorType($wizbiiCreatorType)
    {
        $this->WizbiiCreatorType = $wizbiiCreatorType;
        return $this;
    }

    /**
     * Get wizbiiCreatorType
     *
     * @return string $wizbiiCreatorType
     */
    public function getWizbiiCreatorType()
    {
        return $this->WizbiiCreatorType;
    }

    /**
     * Set wizbiiCreator
     *
     * @param string $wizbiiCreator
     * @return self
     */
    public function setWizbiiCreator($wizbiiCreator)
    {
        $this->WizbiiCreator = $wizbiiCreator;
        return $this;
    }

    /**
     * Get wizbiiCreator
     *
     * @return string $wizbiiCreator
     */
    public function getWizbiiCreator()
    {
        return $this->WizbiiCreator;
    }

    /**
     * Set eventCategory
     *
     * @param string $eventCategory
     * @return self
     */
    public function setEventCategory($eventCategory)
    {
        $this->eventCategory = $eventCategory;
        return $this;
    }

    /**
     * Get eventCategory
     *
     * @return string $eventCategory
     */
    public function getEventCategory()
    {
        return $this->eventCategory;
    }

    /**
     * Set eventAction
     *
     * @param string $eventAction
     * @return self
     */
    public function setEventAction($eventAction)
    {
        $this->eventAction = $eventAction;
        return $this;
    }

    /**
     * Get eventAction
     *
     * @return string $eventAction
     */
    public function getEventAction()
    {
        return $this->eventAction;
    }

    /**
     * Set eventLabel
     *
     * @param string $eventLabel
     * @return self
     */
    public function setEventLabel($eventLabel)
    {
        $this->eventLabel = $eventLabel;
        return $this;
    }

    /**
     * Get eventLabel
     *
     * @return string $eventLabel
     */
    public function getEventLabel()
    {
        return $this->eventLabel;
    }

    /**
     * Set eventValue
     *
     * @param string $eventValue
     * @return self
     */
    public function setEventValue($eventValue)
    {
        $this->eventValue = $eventValue;
        return $this;
    }

    /**
     * Get eventValue
     *
     * @return string $eventValue
     */
    public function getEventValue()
    {
        return $this->eventValue;
    }

    /**
     * Set trackingId
     *
     * @param string $trackingId
     * @return self
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
        return $this;
    }

    /**
     * Get trackingId
     *
     * @return string $trackingId
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set dataSource
     *
     * @param string $dataSource
     * @return self
     */
    public function setDataSource($dataSource)
    {
        $this->dataSource = $dataSource;
        return $this;
    }

    /**
     * Get dataSource
     *
     * @return string $dataSource
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * Set campaignName
     *
     * @param string $campaignName
     * @return self
     */
    public function setCampaignName($campaignName)
    {
        $this->campaignName = $campaignName;
        return $this;
    }

    /**
     * Get campaignName
     *
     * @return string $campaignName
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * Set campaignSource
     *
     * @param string $campaignSource
     * @return self
     */
    public function setCampaignSource($campaignSource)
    {
        $this->campaignSource = $campaignSource;
        return $this;
    }

    /**
     * Get campaignSource
     *
     * @return string $campaignSource
     */
    public function getCampaignSource()
    {
        return $this->campaignSource;
    }

    /**
     * Set campagnMedium
     *
     * @param string $campagnMedium
     * @return self
     */
    public function setCampagnMedium($campagnMedium)
    {
        $this->campagnMedium = $campagnMedium;
        return $this;
    }

    /**
     * Get campagnMedium
     *
     * @return string $campagnMedium
     */
    public function getCampagnMedium()
    {
        return $this->campagnMedium;
    }

    /**
     * Set campaignContent
     *
     * @param string $campaignContent
     * @return self
     */
    public function setCampaignContent($campaignContent)
    {
        $this->campaignContent = $campaignContent;
        return $this;
    }

    /**
     * Get campaignContent
     *
     * @return string $campaignContent
     */
    public function getCampaignContent()
    {
        return $this->campaignContent;
    }

    /**
     * Set screenName
     *
     * @param string $screenName
     * @return self
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;
        return $this;
    }

    /**
     * Get screenName
     *
     * @return string $screenName
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * Set applicationName
     *
     * @param string $applicationName
     * @return self
     */
    public function setApplicationName($applicationName)
    {
        $this->applicationName = $applicationName;
        return $this;
    }

    /**
     * Get applicationName
     *
     * @return string $applicationName
     */
    public function getApplicationName()
    {
        return $this->applicationName;
    }

    /**
     * Set applicationVersion
     *
     * @param string $applicationVersion
     * @return self
     */
    public function setApplicationVersion($applicationVersion)
    {
        $this->applicationVersion = $applicationVersion;
        return $this;
    }

    /**
     * Get applicationVersion
     *
     * @return string $applicationVersion
     */
    public function getApplicationVersion()
    {
        return $this->applicationVersion;
    }

}
