<?php

namespace AnaliticsAPI\Util;

/**
 * Class Constants
 */
abstract class Constants
{
    /**
     * Constante pour la valeur event du HIT TYPE
     */
    const EVENT = 'event';
    /**
     * Datasource web
     */
    const WEB = 'web';
    /**
     * Datasources apps
     */
    const APPS = 'apps';
}